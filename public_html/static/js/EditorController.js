(function() {
    "use strict";

    // links work

    mainApp.controller('EditorController', ['$scope', '$uibModal', 'user', 'ajax', '$rootScope', 'status', '$location', '$route', '$window', '$routeParams', 'dialog',
    function($scope, $uibModal, user, ajax, $rootScope, status, $location, $route, $window, $routeParams, dialog) {

        var cp = $location.search(),
            activePresent = null,
            old_present = {},
            list_id = $routeParams.list_id;

        $scope.options = {
            viewStyle: 'box'
        };

        $scope.$parent.pane = 'Lists';
        $scope.list = [];
        $scope.details = null;
        $scope.refreshed = false;
        $scope.refreshing = false;
        $scope.$emit('pane:loaded', 'lists', "Edit list");

        $scope.timer = function(t) {
            if(t !== undefined) {
                return moment(t).fromNow();
            }
            else {
                return '';
            }
        };

        $scope.showPic = function(e, p) {
            if(p.present_haspic) {
                $location.path('showpic/' + p.present_id);
                e.stopPropagation();
            }
        };

        $scope.edit = function() {
            $uibModal.open({
                animation: true,
                templateUrl: '/static/html/listModal.html',
                controller: 'ListModalInstanceController',
                backdrop: false,
                resolve: {
                    details: function () {
                        var d = angular.copy($scope.details);
                        d.msg = 'Rename list ' + d.list_name;
                        return d;
                    }
                }
            }).result.then(function (result) {
                $scope.details = angular.copy(result);
            });
        };

        function shareList(email) {
        }

        $scope.share = function() {
            $location.path('/share/' + list_id);
        };

        $scope.delete = function() {
            $location.path('/lists');
            ajax.delete('list/' + list_id)
            .then(function(response) {
                var t;
                toastr.options = {
                    closeButton: true,
                    positionClass: "toast-top-center",
                    timeOut: "0",
                    extendedTimeOut: "10000"
                };
                t = toastr.info('<p>' + $scope.details.list_name + ' has been deleted.&nbsp;<button id="undoDeleteButton" class="btn btn-sm btn-primary">Undo</button></p>', "Deleted" );
                if(t.find('#undoDeleteButton').length) {
                    t.delegate('#undoDeleteButton', 'click', function () {
                    ajax.post('list/undelete/' + list_id)
                    .then(function(response) {
                        $rootScope.$broadcast('lists-changed');
                        t.remove();
                    });
                });
            }
            });
        };

        $scope.toggle = function(event, present) {
            if(activePresent !== null && activePresent !== present) {
                if(!angular.equals(activePresent, old_present)) {
                    // Send changes to the server here
                }
//                $('#present' + activePresent.present_id).collapse('hide');
                activePresent.active = false;
            }
            present.active = !present.active;
            activePresent = present;
            old_present = angular.copy(activePresent);
            $('#present' + present.present_id).collapse('toggle');
        };

        $scope.nope = function(event, present) {
            event.stopPropagation();
        };

        $scope.editItDialog = function(event, present) {
            event.stopPropagation();
            $uibModal.open({
                animation: true,
                templateUrl: '/static/html/presentModal.html',
                controller: 'PresentModalInstanceController',
                backdrop: false,
                resolve: {
                    details: function () {
                        return {
                            msg:'Edit present',
                            present_id: present.present_id,
                            present_name: present.present_name,
                            present_description: present.present_description,
                            present_link: present.present_link,
                            present_haspic: present.present_haspic,
                            list_id: list_id
                        };
                    }
                }
            }).result.then(function() {
                refreshList();
            }, function() {
                // cancelled
            });
        };

        // create a new present
        // and a link to the current list
        //

        $scope.addPresent = function(event) {
            event.stopPropagation();
            user.login('Login to create a present (you should not be seeing this...')
            .then(function() {
                $uibModal.open({
                    animation: true,
                    templateUrl: '/static/html/presentModal.html',
                    controller: 'PresentModalInstanceController',
                    backdrop: false,
                    resolve: {
                        details: function () {
                            return {
                                msg:'New present',
                                present_id: 0,
                                present_name: '',
                                present_description: null,
                                present_link: null,
                                present_haspic: 0,
                                list_id: list_id
                            };
                        }
                    }
                }).result.then(function() {
                    refreshList();  // TODO (chs): cache the list locally in a service and don't keep downloading the whole thing
                }, function() {
                    // cancelled
                });
            });
        };

        $scope.remove = function(event, present) {
            event.stopPropagation();
            ajax.post('link/delete/' + present.present_id + "/" + list_id)
            .then(refreshList);  // TODO (chs): cache the list locally and don't download the whole thing again
        };

        refreshList();

        function refreshList() {
            $scope.refreshing = true;
            user.login()
            .then(function() {
                ajax.get('list/mine/' + $routeParams.list_id)
                .then(function(response) {
                    $scope.list = response.data.presents;
                    $scope.details = response.data.details;
                    $scope.refreshed = true;
                    $scope.refreshing = false;
                }, function(response) {
                    $location.path('/lists');
                });
            }, function() {
                $location.path('/view/' + list_id);        // try and view it. if it's not public or shared with currently logged in user, they'll get redirected to the root
            });
        }

    }]);
})();
