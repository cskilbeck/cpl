(function() {
    "use strict";

    mainApp.controller('ShareController', ['$scope', '$uibModal', 'user', 'ajax', '$rootScope', 'status', '$location', '$route', '$window', '$routeParams', 'dialog', '$timeout',
    function($scope, $uibModal, user, ajax, $rootScope, status, $location, $route, $window, $routeParams, dialog, $timeout) {

        var list_id = $routeParams.list_id;

        $scope.$emit('pane:loaded', 'lists', "Sharing");

        $scope.failed = false;
        $scope.shares = [];
        $scope.list_name = '';

        $scope.cancel = function() {
            $window.history.back();
        };

        $scope.activeShares = function() {
            return $scope.shares.filter(function(s) {
                return s.action !== 'delete';
            });
        };

        $scope.addShare = function() {
            $('#shareEmail').val('');
            $scope.adding = true;
            $timeout(function() {
                $('#shareEmail').focus();
            }, 100);
        };

        $scope.pressed = function(ev, email) {
            if(ev.keyCode === 13) {
                $scope.added(email);
            }
        };

        $scope.added = function(email) {
            var addr;
            if(email.$valid) {
                addr = email.shareEmail.$modelValue;
                if($scope.shares.find(function(s) {
                    return s.user_email === addr;
                }) === undefined) {
                    $scope.shares.push({
                        user_id: 0,
                        user_username: null,
                        user_email: addr,
                        action: 'add'
                    });
                    email.$setPristine();
                    $scope.adding = false;
                }
                else {
                    toastr.options = {
                        closeButton: true,
                        hideDuration: 500,
                        positionClass: "toast-top-center",
                        timeOut: "5000",
                        extendedTimeOut: "10000"
                    };
                    toastr.info('<p>' + "Already sharing with " + addr + '</p>', "Duplicate email" );
                }
            }
        };

        $scope.deleteShare = function(share) {
            if(share.action !== 'add') {
                share.action = 'delete';
            }
            else {
                $scope.shares = $scope.shares.filter(function(s) {
                    return s.user_email !== share.user_email;
                });
            }
            $scope.adding = false;
        };

        $scope.cancelEntry = function() {
            $scope.adding = false;
            $('#share-email').val('');
        };

        $scope.username = function(share) {
            if(share.user_username === null || share.user_username.length === 0) {
                return '';
            }
            return share.user_username + ": ";
        };

        $scope.done = function() {
            var diff = $scope.shares.reduce(function(l, s) {
                if(s.action) {
                    l[s.action].push(s.user_email);
                }
                return l;
            }, { add: [], delete: [] });

            if(diff.add.length || diff.delete.length) {
                ajax.post('list/share/' + list_id, {
                    add: diff.add.join(";"),
                    del: diff.delete.join(";")
                }).then(function(response) {
                    console.log(response);
                    $window.history.back();
                }, function(response) {
                    console.log(response);
                });
            }
            else {
                $window.history.back();
            }
        };

        $scope.refreshShares = function() {
            $scope.refreshing = true;
            ajax.get('list/sharedwith/' + list_id)
            .then(function(response) {
                $scope.refreshing = false;
                console.log(response.data);
                $scope.shares = response.data.shares;
                $scope.list_name = response.data.list_name;
            }, function(response) {
                $scope.refreshing = false;
                $scope.failed = true;
            });
        };

        $scope.refreshShares();

    }]);

})();

