(function() {
    "use strict";

    var orders = [
            { ord: 4, dir: 0, text: "Rating" },
            { ord: 0, dir: 0, text: "Saved" },
            { ord: 1, dir: 0, text: 'Created' },
            { ord: 4, dir: 1, text: 'Played' }      // TODO (chs) implement game_playcount
        ],
        oldOptions = {};

    mainApp.controller('ListsController', ['$scope', '$uibModal', 'user', 'ajax', '$rootScope', 'status', '$location', '$route', '$window', '$timeout', '$routeParams',
    function($scope, $uibModal, user, ajax, $rootScope, status, $location, $route, $window, $timeout, $routeParams) {

        function refreshLists() {
            if($routeParams.dontlogin) {
                $scope.notloggedin = true;
                $scope.lists = [];
                $scope.refreshing = false;
                return;
            }
            $scope.refreshing = true;
            var q = Q.defer();
            user.login()
            .then(function() {
                $scope.notloggedin = false;
                ajax.get('list')
                .then(function(response) {
                    $scope.lists = response.data;
                    $scope.refreshing = false;
                    if(response.data.length === 0) {
                        $timeout(function() {
                             $('#newlistbutton').popover({
                                title: "You have no lists!",
                                content: "Click here to create one",
                                placement: "bottom",
                                trigger: "manual"
                            });
                            $('#newlistbutton').popover("show");
                        }, 1000);
                    }
                    q.resolve();
                }, function(response) {
                    q.reject();
                });
            }, function() {
                $scope.refreshing = false;
                $scope.notloggedin = true;
                q.reject();
            });
            return q.promise;
        }

        $scope.login = function() {
            $scope.notloggedin = false;
            $location.search({ dontlogin: null });
            $route.reload();
        };

        $scope.options = {
            viewStyle: 'box'
        };

        $scope.$parent.pane = 'Lists';
        $scope.lists = [];
        $scope.$emit('pane:loaded', 'lists', "Your lists");
        $scope.moment = moment;

        $scope.popItUp = function(event, list_id) {
            $location.path('/edit/' + list_id);
            event.preventDefault();
        };

        $scope.newlist = function() {
            $('#newlistbutton').popover("destroy");
            user.newlist();
        };

        $scope.$on('user:login', function() {
            $scope.notloggedin = false;
            $location.search({});
            $route.reload();
            refreshLists();
        });

        $scope.$on('lists-changed', function() {
            $('#newlistbutton').popover("destroy");
            refreshLists().then(function() {
                $scope.$apply();
            });
        });

        refreshLists();

    }]);
})();