(function() {
    "use strict";

    mainApp.config(['$routeProvider', '$locationProvider',
    function($routeProvider, $locationProvider) {
        $routeProvider.when('/', {
            templateUrl: '/static/html/home.html',
            controller: 'HomeController'
        }).when('/lists', {
            templateUrl: '/static/html/lists.html',
            reloadOnSearch: false,
            controller: 'ListsController'
        }).when('/edit/:list_id', {
            templateUrl: '/static/html/editor.html',
            controller: 'EditorController'
        }).when('/share/:list_id', {
            templateUrl: '/static/html/share.html',
            controller: 'ShareController'
        }).when('/shop', {
            templateUrl: '/static/html/shop.html',
            controller: 'ShopController'
        }).when('/view', {
            templateUrl: '/static/html/view.html',
            controller: 'ViewController'
        }).when('/view/:list_id', {
            templateUrl: '/static/html/viewer.html',
            controller: 'ViewerController'
        }).when('/showpic/:present_id', {
            templateUrl: '/static/html/showPic.html',
            controller: 'ShowPicController'
        }).when('/help', {
            templateUrl: '/static/html/help.html',
            controller: 'HelpController'
        }).otherwise({
            redirectTo: '/'
        });
        $locationProvider.html5Mode(true);
    }]);

})();
