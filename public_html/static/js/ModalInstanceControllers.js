(function() {
    "use strict";

    var extMap = {
        JPEG: 'jpg',
        BMP: 'bmp',
        PNG: 'png'
    };

    mainApp.controller('LoginModalInstanceController', ['$scope', '$uibModal', '$modalInstance', 'details', 'ajax', 'user', 'status', '$timeout', 'util',
    function ($scope, $uibModal, $modalInstance, details, ajax, user, status, $timeout, util) {

        $scope.details = details;
        $scope.details.failed = false;
        $scope.loginInProgress = false;

        $timeout(function() {
            util.focus($('#email').focus());
        }, 350);

        $scope.ok = function () {
            $scope.loginInProgress = true;
            $scope.details.failed = false;
            user.dologin($scope.details)
            .then(function(data) {
                $modalInstance.close(data);
                status(user.name() + " signed in");
            }, function(response) {
                $scope.loginInProgress = false;
                status.error('Login failed', 3);
                $scope.details.failed = true;
                $scope.$apply();
            });
        };

        $scope.cancel = function () {
            $modalInstance.dismiss('cancelled');
        };

        $scope.showRegistration = function () {
            $modalInstance.dismiss('registration-required');
        };

        $scope.resetpw = function() {
            $scope.loginInProgress = true;
            ajax.post('user/resetpw', { email: details.email })
            .then(function(response) {
                $modalInstance.dismiss('resetpassword-complete');
            }, function(response) {
                $modalInstance.dismiss('resetpassword-failed');
            });
        };

    }]);

    mainApp.controller('RegisterModalInstanceController', ['$scope', '$uibModal', '$modalInstance', 'details', 'ajax', 'user', '$timeout', 'status', 'util',
    function ($scope, $uibModal, $modalInstance, details, ajax, user, $timeout, status, util) {

        $scope.details = details;
        $scope.message = 'Fill in required fields...';
        $scope.registrationInProgress = false;
        $scope.details.failed = false;

        // details.needOldPassword - true if showProfilebutton, false if register or resetpassword
        // details.changePassword - true if register or resetpassword or

        $timeout(function() {
            util.focus($('#username'));
        }, 350);

        $scope.resetpw = function() {
            $scope.loginInProgress = true;
            ajax.post('user/resetpw', { email: details.email })
            .then(function(response) {
                $modalInstance.dismiss('resetpassword-complete');
            }, function(response) {
                $modalInstance.dismiss('resetpassword-failed');
            });
        };

        $scope.showChangePasswordCheck = !($scope.registrationInProgress || $scope.details.update) && $scope.details.editingProfile;

        $scope.ok = function () {

            $scope.registrationInProgress = true;
            $scope.details.failed = false;
            if(!$scope.details.changePassword) {
                delete $scope.details.password;
                delete $scope.details.password2;
            }
            if(!$scope.details.editingProfile) {
                delete $scope.details.oldpassword;
            }
            user.register($scope.details)
            .then(function(response) {
                $modalInstance.close(response.data);
            }, function(response) {
                $scope.details.failed = response.status;
                $scope.registrationInProgress = false;
                $scope.message = response.statusText;
                $scope.$apply();
            });
        };

        $scope.cancel = function () {
            $modalInstance.dismiss('cancelled');
        };

        $scope.passmatch = function() {
            return $scope.details.password === $scope.details.password2;
        };
    }]);

    mainApp.controller('ListModalInstanceController', ['$scope', '$uibModal', '$modalInstance', 'details', 'ajax', 'user', '$timeout', 'status', 'util',
    function ($scope, $uibModal, $modalInstance, details, ajax, user, $timeout, status, util) {
        $scope.details = details;
        $scope.saveInProgress = false;
        $scope.details.failed = false;

        $timeout(function() {
            util.focus($('#list_name'));
        }, 350);

        $scope.ok = function() {
            $scope.details.errormessage = '';
            $scope.saveInProgress = true;
            $scope.details.failed = false;
            var p,
                d = {
                    name: details.list_name,
                    description: details.list_description,
                    public: details.list_public
                };
            if(details.list_id) {
                p = ajax.put('list/' + details.list_id, d);
            }
            else {
                p = ajax.post('list', d);
            }
            p.then(function(response) {
                details.list_id = response.data.list_id;
                $modalInstance.close(details);
            }, function(response) {
                $scope.saveInProgress = false;
                $scope.details.failed = true;
                $scope.details.errormessage = response.statusText;
                $timeout(function() {
                    $scope.details.errormessage = '';
                    $scope.details.failed = false;
                }, 2000);
            });
        };

        $scope.cancel = function () {
            $modalInstance.dismiss('cancelled');
        };
    }]);

    mainApp.controller('ShareModalInstanceController', ['$scope', '$uibModal', '$modalInstance', 'details', 'ajax', 'user', '$timeout', 'status', 'util',
    function ($scope, $uibModal, $modalInstance, details, ajax, user, $timeout, status, util) {
        $scope.details = details;
        $scope.shareInProgress = false;
        $scope.refreshing = true;
        $scope.details.failed = false;
        $scope.shares = [];

        // {
        //      recipient: user_id of how it's shared with,
        //      user_email: email of who it's shared with
        //      user_username: name of who it's shared with
        //      action: ['add' | 'remove' ]
        // }

        $timeout(function() {
            util.focus($('#shareEmail'));
        }, 350);

        $scope.ok = function () {
            $scope.shareInProgress = true;
            $scope.details.failed = false;
            ajax.post('list/share/' + $scope.details.list_id, { email: $scope.details.email, name: $scope.details.name })
            .then(function(response) {
                $scope.shareInProgress = false;
                $modalInstance.close(response.data);
            }, function(response) {
                $scope.details.failed = true;
                $scope.shareInProgress = false;
            });
        };

        $scope.cancel = function () {
            $modalInstance.dismiss('cancelled');
        };

        // Get the users that this list is shared with
        ajax.get('list/sharedwith/' + details.list_id)
        .then(function(response) {
            $scope.refreshing = false;
            $scope.shares = response.data;
        }, function(response) {
            $scope.refreshing = false;
            $scope.details.failed = true;
        });

    }]);

// POST /api/present - new present (need a list ID to put it in)
// PUT /api/present/{present_id} - change a present
// GET /api/presents/{list_id} - get presents in a list

    mainApp.controller('PresentModalInstanceController', ['$scope', '$uibModal', '$modalInstance', 'details', 'ajax', 'user', '$timeout', 'status', 'util',
    function ($scope, $uibModal, $modalInstance, details, ajax, user, $timeout, status, util) {

        $scope.saveInProgress = false;

        $scope.details = details;

        details.failed = false;
        details.present_pic = null;
        details.update_pic = 0;

        $timeout(function() {
            util.focus($('#present_name'));
        }, 350);

        $scope.getPhoto = function(e) {
            $('#choosePhoto')[0].click();
        };

        $scope.$watch('details.present_pic', function(nv, ov) {
            if(nv) {
                console.log("PIC!!", nv);
                details.update_pic = 1;
                $('#presentpicthumbnail').attr('src', nv);
            }
        });

        $scope.noPhoto = function() {
            return details.present_pic === null && !details.present_haspic;
        };

        $scope.clearPhoto = function() {
            details.present_pic = null;
            details.present_haspic = 0;
            details.update_pic = 1;
            $('#presentpicthumbnail').attr('src', '');
            $('#choosePhoto').val('');
        };

        $scope.pic = function() {
            if(details.present_id && details.present_haspic) {
                return 'thumbnails/' + $scope.details.present_id;
            }
            else {
                return details.present_pic;
            }
        };

        function save_present(details) {
            var q = Q.defer(),
                d = {
                    present_name: details.present_name,
                    present_description: details.present_description,
                    present_link: details.present_link,
                    present_pic: details.present_pic,
                    update_pic: details.update_pic,
                    list_id: details.list_id
                },
                p = details.present_id ? ajax.put('present/' + details.present_id, d) : ajax.post('present', d);
            p.then(function(response) {
                q.resolve(response);
            }, function(response) {
                q.reject(response);
            });
            return q.promise;
        };

        $scope.ok = function() {
            $scope.details.errormessage = '';
            $scope.saveInProgress = true;
            $scope.details.failed = false;
            save_present($scope.details)
            .then(function(response) {
                details.present_id = response.data.present_id;
                $modalInstance.close(details);
            }, function(response) {
                $scope.saveInProgress = false;
                $scope.details.failed = true;
                $scope.details.errormessage = response.statusText;
                $timeout(function() {
                    $scope.details.errormessage = '';
                    $scope.details.failed = false;
                }, 2000);
            });
        };

        $scope.cancel = function () {
            $modalInstance.dismiss('cancelled');
        };
    }]);

    mainApp.controller('DialogModalInstanceController', ['$scope', '$uibModal', '$modalInstance', 'options', '$timeout',
    function ($scope, $uibModal, $modalInstance, options, $timeout) {

        $scope.options = options;

        // DONE (chs): make input field focus on dialog show work!? [$uibModal was focusing element 0 in a requestAnimationFrame after the animation]
        // $modalInstance.opened.then(function() {
        //     $timeout(function() {
        //         util.focus($('#input'));
        //     }, 350);
        // });

        $scope.ok = function() {
            $modalInstance.close();
        };

        $scope.cancel = function() {
            $modalInstance.dismiss();
        };

    }]);

})();

