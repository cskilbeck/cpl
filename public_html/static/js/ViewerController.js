(function() {
    "use strict";

    mainApp.controller('ViewerController', ['$scope', '$uibModal', 'user', 'ajax', '$rootScope', 'status', '$location', '$route', '$window', '$routeParams', 'dialog',
    function($scope, $uibModal, user, ajax, $rootScope, status, $location, $route, $window, $routeParams, dialog) {

        var q = $location.search(),
                activePresent = null;

        function available_list() {
            return $scope.list.filter(function(p) {
                return p.present_reserved_by === null;
            });
        }

        function reserved_by_me_list() {
            return $scope.list.filter(function(p) {
                return p.present_reserved_by === user.id();
            });
        }

        function reserved_list() {
            return $scope.list.filter(function(p) {
                return p.present_reserved_by !== null && p.present_reserved_by !== user.id();
            });
        }

        $scope.showPic = function(e, p) {
            if(p.present_haspic) {
                $location.path('showpic/' + p.present_id);
                e.stopPropagation();
            }
        };

        function refresh(full) {
            $scope.refreshList(full);
        }

        $scope.refreshList = function(full) {
            $scope.refreshing = (full === undefined) ? true : full;
            ajax.get('list/view/' + $routeParams.list_id)
            .then(function(response) {
                $scope.list = response.data.list;
                $scope.reserved = reserved_list();
                $scope.available = available_list();
                $scope.shopping = reserved_by_me_list();
                $scope.details = response.data.details;
                $scope.refreshing = false;
            }, function(response) {
                $location.path('/lists');
            });
        };

        $scope.toggle = function(event, present) {
            if(activePresent !== null && activePresent !== present) {
                activePresent.active = false;
            }
            present.active = !present.active;
            activePresent = present;
        };

        $scope.viewIt = function(event, present) {
        };

        $scope.reserve = function(event, present) {
            present.present_reserved_by = user.id();
            present.active = false;
            activePresent = null;
            ajax.post('present/reserve/' + present.present_id)
            .then(function() {
            }, function() {
                refresh(false);
            });
        };

        $scope.release = function(event, present) {
            present.present_reserved_by = null;
            present.active = false;
            activePresent = null;
            ajax.post('present/release/' + present.present_id)
            .then(function() {
            },
            function() {
                refresh(false);
            });
        };

        $scope.options = {
            viewStyle: 'box'
        };

        $scope.areas = [
            {
                banner: 'Reserved by me',
                list: reserved_by_me_list,
                buttonText: 'Release',
                buttonFunction: $scope.release,

            },
            {
                banner: 'Available',
                list: available_list,
                buttonText: 'Bag it!',
                buttonFunction: $scope.reserve
            },
            {
                banner: 'Taken',
                list: reserved_list,
                buttonText: null,
                buttonFunction: null
            }
        ];

        $scope.$parent.pane = 'Lists';
        $scope.list = [];
        $scope.details = null;
        $scope.$emit('pane:loaded', 'view', "View list");
        $scope.refreshing = true;

        // got a share code?
        if(q.code) {

            var code_id, id;

            // yes, get the share code details
            ajax.get('user/fromcode/' + q.code)
            .then(function(response) {

                // got an account waiting to log back in?
                id = user.token_id();
                if(id !== 0) {

                    // yes, log them in
                    user.login()
                    .then(function() {

                        // same user?
                        if(user.email() === response.data.user_email) {
                            ajax.post('user/activate/' + q.code)
                            .then(function(response) {
                                // yes, fine, show it
                                $scope.refreshList();
                            }, function(response) {
                                $scope.refreshList();
                            });
                        }
                        else {
                            // OK, well, then tell the server to share with this user instead (modify the share record)
                            ajax.post('user/sharechange/' + q.code, {})
                            .then(function(response) {
                                // share changed
                                $scope.refreshList();
                            }, function(response) {
                                // show an error dialog, then... what?
                                $location.path('/lists');
                            });
                        }
                    }, function() {
                        $location.path('/lists');
                    });
                }
                else {
                    // shared with a new user
                    // pop the registration dialog in special half-formed user mode
                    response.data.dialogTile = 'Create your account';
                    response.data.code = q.code;
                    user.createProfile(response.data)
                    .then(function(details) {
                        $scope.refreshList();
                    }, function() {
                        $scope.refreshList();
                    });
                }
            }, function() {
                $location.path('/lists');
            });
        }
        else {
            $scope.refreshList();
        }

    }]);
})();
