mainApp.factory('user', [ '$rootScope', '$uibModal', 'ajax', 'status', 'dialog', '$location', 'util',
function ($rootScope, $uibModal, ajax, status, dialog, $location, util) {
    "use strict";

    var details = {
            user_id: 0,
            user_username: "",
            user_email: ""
        },
        refreshing = false,

        expire = function (days) {
            var now = new Date();
            return {
                expires: new Date(now.getFullYear(), now.getMonth() + 1, now.getDate())
            };
        },

        save_token = function(token) {
            localStorage.setItem('token', token);
        },

        delete_token = function() {
            localStorage.removeItem('token');
        },

        get_token = function() {
            return localStorage.getItem('token');
        },

        get_user_id_from_token = function() {
            var payload = util.parse_jwt(get_token());
            if(payload) {
                return payload.user_id;
            }
            return 0;
        },

        handleReason = function(reason, loginDetails) {
            var q = Q.defer();
            switch(reason) {
                case 'registration-required':
                    loginDetails.dialogTitle = 'Register';
                    loginDetails.editingProfile = false;
                    loginDetails.changePassword = true;
                    loginDetails.update = false;
                    $uibModal.open({
                        animation: true,
                        size: 'x-small',
                        templateUrl: '/static/html/registerModal.html',
                        controller: 'RegisterModalInstanceController',
                        resolve: {
                            details: function() {
                                return loginDetails;
                            }
                        }
                    }).result.then(function(result) {
                        save_token(result.token);
                        user.update(result, 'register');
                        q.resolve(result);
                    }, function(reason) {
                        q.reject();
                    });
                    break;
                case 'resetpassword-complete':
                    dialog.small.inform("Password reset", "We've sent an email to " + loginDetails.email + " with instructions for resetting your password.\n\nThanks!")
                    .then(function() {
                        q.reject();
                    });
                    break;
                case 'resetpassword-failed':
                    dialog.small.inform("Password reset failed", "Sorry, we don't seem to have a record of " + loginDetails.email + " in our system, are you sure you have the right address?")
                    .then(function() {
                        q.reject();
                    });
                    break;
                default:
                    q.reject();
                }
                return q.promise;
            },

        user = {

            isLoggedIn: function() {
                return details.user_id !== 0;
            },

            register: function(details) {
                if(details.code) {
                    return ajax.post('/user/createprofile', details);
                }
                if(details.update || details.editingProfile) {
                    return ajax.post('user/modify', details, 'Updating details for ' + details.email);
                }
                else {
                    return ajax.post('user/register', details, 'Registering ' + details.email);
                }
            },

            dologin: function(details) {
                var q = Q.defer();
                ajax.post('user/login', details, 'Logging in ' + details.email + '...')
                .then(function(response) {
                    q.resolve(response.data);
                }, function(response) {
                    q.reject(response);
                });
                return q.promise;
            },

            newlist: function() {
                user.login('Login to create a list')
                .then(function() {
                    $uibModal.open({
                        animation: true,
                        templateUrl: '/static/html/listModal.html',
                        controller: 'ListModalInstanceController',
                        backdrop: false,
                        resolve: {
                            details: function () {
                                return {
                                    msg:'New Christmas List',
                                    list_id: 0,
                                    list_name: '',
                                    list_description: null
                                };
                            }
                        }
                    }).result.then(function (result) {
                        $location.path('/edit/' + result.list_id);
                    }, function() {

                    });
                }, function() {

                });
            },

            do_refresh: function() {
                var q = Q.defer(), ref, exp, user_token = get_token();
                if(user_token) {
                    if(refreshing) {
                        ref = $rootScope.$on('session-refreshed', function() {
                            ref();
                            q.resolve();
                        });
                        exp = $rootScope.$on('session-expired', function() {
                            exp();
                            q.resolve();
                        });
                    }
                    else {
                        refreshing = true;
                        ajax.post('user/refreshSession')
                        .then(function(response) {
                            save_token(response.data.token);
                            user.update(response.data, 'refreshSession');
                            refreshing = false;
                            $rootScope.$broadcast('session-refreshed');
                            status('Welcome back ' + response.data.user_username);
                            q.resolve();
                        },
                        function(response) {
                            user.update({user_id: 0}, 'sessionExpired');
                            refreshing = false;
                            $rootScope.$broadcast("session-expired");
                            status('Session expired, please log in again...');
                            q.resolve();
                        });
                    }
                }
                else {
                    q.reject();
                }
                return q.promise;
            },

            login_user: function (message) {
                var loginDetails =  {
                    username: '',
                    email: '',
                    password: '',
                    password2: '',
                    failed: false,
                    msg: message || "Sign In"
                };

                var q = Q.defer();
                if(details.user_id === 0) {

                    $uibModal.open({
                        animation: true,
                        size: 'x-small',
                        templateUrl: '/static/html/loginModal.html',
                        controller: 'LoginModalInstanceController',
                        resolve: {
                            details: function () {
                                return loginDetails;
                            }
                        }
                    }).result
                    .then(function (result) {
                        save_token(result.token);
                        user.update(result, 'login');
                        q.resolve(result);
                        $rootScope.$broadcast('user:login');
                    }, function(reason) {
                        handleReason(reason, loginDetails)
                        .then(function(result) {
                            q.resolve(result);
                            $rootScope.$broadcast('user:login');
                        }, function() {
                            q.reject();
                        });
                    });
                } else {
                    q.resolve(details);
                }
                return q.promise;
            },

            login: function(message) {
                var user_token = get_token(),
                    ref,
                    q = Q.defer();
                if(details.user_id === 0) {
                    if(user_token) {
                        return user.do_refresh();
                    }
                    else {
                        return user.login_user(message);
                    }
                }
                else {
                    q.resolve();
                }
                return q.promise;
            },

            editProfile: function() {
                var q = Q.defer(),
                    details = {
                                username: user.name(),
                                uid: user.id(),
                                email: user.email(),
                                editingProfile: true,
                                update: false,
                                changePassword: false,
                                newPasswordTitle: "New ",
                                oldpassword: '',
                                password: '',
                                password2: '',
                                failed: false,
                                msg: "Account details",
                                dialogTitle: "Account details"
                            };
                $uibModal.open({
                    animation: true,
                    size: 'x-small',
                    templateUrl: '/static/html/registerModal.html',
                    controller: 'RegisterModalInstanceController',
                    resolve: {
                        details: function() {
                            return details;
                        }
                    }
                }).result.then(function(result) {
                    user.update(result, 'profile');
                    q.resolve(result);
                }, function(reason) {
                    handleReason(reason, details)
                    .then(function() {
                        q.reject();
                    });
                });
                return q.promise;
            },

            createProfile: function(profile) {
                var q = Q.defer(),
                    details = {
                        uid: profile.user_id,
                        code: profile.code,
                        email: profile.user_email,
                        username: profile.user_username,
                        dialogTitle: 'Create Account',
                        emailFixed: true,
                        editingProfile: false,
                        changePassword: true,
                        update: false
                    };
                $uibModal.open({
                    animation: true,
                    size: 'x-small',
                    templateUrl: '/static/html/registerModal.html',
                    controller: 'RegisterModalInstanceController',
                    resolve: {
                        details: function() {
                            return details;
                        }
                    }
                }).result.then(function(result) {
                    save_token(result.token);
                    user.update(result, 'register');
                    q.resolve(result);
                }, function(reason) {
                    q.reject();
                });
                return q.promise;
            },

            refreshSession: function(query) {
                var user_token = get_token(),
                    q = Q.defer();

                if(query &&
                    Object.prototype.hasOwnProperty.call(query, 'resetpassword') &&
                    Object.prototype.hasOwnProperty.call(query, 'email')) {
                    // they want to reset their password - pop up the register dialog so they can change their profile
                    var details =  {
                        username: '',
                        user_id: 0,
                        email: query.email,
                        code: query.resetpassword,
                        changePassword: true,
                        password: '',
                        password2: '',
                        failed: false,
                        msg: "Edit details",
                        dialogTitle: 'Edit your details'
                    };
                    // get username from database 1st
                    ajax.get('user/details', {
                        code: query.resetpassword,
                        email: query.email
                    })
                    .then(function(response) {
                        $uibModal.open({
                            animation: true,
                            size: 'x-small',
                            templateUrl: '/static/html/registerModal.html',
                            controller: 'RegisterModalInstanceController',
                            resolve: {
                                details: function() {
                                    details.update = true;
                                    details.editingProfile = true;
                                    details.username = response.data.user_username;
                                    details.uid = response.data.uid;
                                    return details;
                                }
                            }
                        }).result.then(function(result) {
                            save_token(result.token);
                            user.update(result, 'register');
                            q.resolve(result);
                        }, function(reason) {
                            // couldn't get user details
                            q.reject();
                        });
                    }, function(response) {
                        dialog.small.inform("Invalid reset code", "Sorry, no dice - reset code has expired or is invalid or email address is invalid");
                        q.reject();
                    });
                }
                else if(user_token) {
                    return user.do_refresh();
                }
                else {
                    q.resolve();
                }
                return q.promise;
            },

            logout: function() {
                var q = Q.defer();
                delete_token();
                details = {
                            user_id: 0,
                            user_username: "",
                            user_email: ""
                        };
                q.resolve();
                $rootScope.$broadcast('user:logout');
                return q.promise;
            },

            update: function(d, reason) {
                details = d;
                if(details.user_id !== 0) {
                    $rootScope.$broadcast('user:updated', details);
                }
            },

            token_id: function() {
                return get_user_id_from_token();
            },

            id: function() {
                return details.user_id;
            },

            name: function() {
                return details.user_username || "Anonymous";
            },

            email: function() {
                return details.user_email;
            }
        };

    return user;

}]);

