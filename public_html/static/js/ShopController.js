(function() {
    "use strict";

    mainApp.controller('ShopController', ['$scope', '$uibModal', 'user', 'ajax', '$rootScope', 'status', '$location', '$route', '$window', '$routeParams', 'dialog',
    function($scope, $uibModal, user, ajax, $rootScope, status, $location, $route, $window, $routeParams, dialog) {

        function refresh() {
            $scope.refreshList();
        }

        $scope.refreshList = function() {
            $scope.refreshing = true;
            user.login()
            .then(function() {
                ajax.get('list/shop')
                .then(function(response) {
                    $scope.list = response.data;
                    $scope.refreshing = false;
                }, function(response) {
                    $location.path('/lists');
                });
            }, function() {
                $location.path('/lists').search({ dontlogin: true });
            });
        };

        $scope.viewIt = function(event, present) {
            // follow link if there is one, else show dialog
            // with image
        };

        $scope.options = {
            viewStyle: 'box'
        };

        $scope.showPic = function(e, p) {
            if(p.present_haspic) {
                $location.path('showpic/' + p.present_id);
                e.stopPropagation();
            }
        };

        $scope.$parent.pane = 'Shop';
        $scope.list = [];
        $scope.$emit('pane:loaded', 'shop', "Shopping");

        refresh();

    }]);
})();
