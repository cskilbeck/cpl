(function() {
    "use strict";

    mainApp.controller('ViewController', ['$scope', '$uibModal', 'user', 'ajax', '$rootScope', 'status', '$location', '$route', '$window',
    function($scope, $uibModal, user, ajax, $rootScope, status, $location, $route, $window) {

        function refreshLists() {
            var q = Q.defer();
            $scope.refreshing = true;
            user.login()
            .then(function() {
                ajax.get('list/shared')
                .then(function(response) {
                    $scope.lists = response.data;
                    $scope.refreshing = false;
                    q.resolve();
                }, function(response) {
                    q.reject();
                });
            }, function() {
                q.reject();
                $location.path("/lists").search({ dontlogin: true });
            });
            return q.promise;
        }

        $scope.options = {
            viewStyle: 'box'
        };

        $scope.$parent.pane = 'View';
        $scope.lists = [];
        $scope.$emit('pane:loaded', 'view', "View shared lists");

        $scope.timer = function(t) {
            if(t) {
                return moment(t).fromNow();
            }
            else {
                return '-';
            }
        };

        $scope.viewIt = function(event, list_id) {
            $location.path('/view/' + list_id);
            event.preventDefault();
        };

        $scope.$on('lists-changed', function() {
            refreshLists().then(function() {
                $scope.$apply();
            });
        });

        refreshLists();

    }]);
})();