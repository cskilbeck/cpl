mainApp.controller('MainController', ['$scope', '$uibModal', 'user', 'ajax', '$rootScope', 'status', '$location', '$route', '$window',
function($scope, $uibModal, user, ajax, $rootScope, status, $location, $route, $window) {
    "use strict";

    var signInMessage = "Sign in or Register";

    $scope.status = status;
    $scope.signInMessage = signInMessage;
    $scope.pane = '';
    $scope.user_id = user.id();
    $scope.showProfileButton = false;
    $scope.usernameMessage = "";
    $scope.showBackdropper = false;

    function clearNavBar() {
        $('.nav.navbar-nav > li').removeClass('active');
    }

    $scope.alert = {
        text: '',
        header: '',
        button_text: null
    };

    $scope.alert_clicked = function() {
    };

    $scope.newList = function() {
        user.newlist();
    };

    $scope.$on('showBackdropper', function() {
        $scope.showBackdropper = true;
    });

    $scope.$on('hideBackdropper', function() {
        $scope.showBackdropper = false;
    });

    $scope.backdropperClicked = function() {
        $scope.showBackdropper = false;
    };

    $('#homelink').on('click', clearNavBar);

    $scope.$on('pane:loaded', function(msg, pane, text) {
        clearNavBar();
        $('#nav' + pane).addClass('active');
        $rootScope.bannerText = text || '-';
    });

    $scope.$on('user:updated', function(msg, details) {
        if(details.user_id !== 0) {
            $scope.usernameMessage = 'Signed in as ' + details.user_username;
            $scope.signInMessage = "Sign out";
            status("Welcome back " + details.user_username);
            $scope.showProfileButton = true;
        }
    });

    $scope.$on('user:logout', function(msg) {
        $scope.usernameMessage = '';
        $scope.signInMessage = signInMessage;
        status("Signed out");
        $scope.showProfileButton = false;
    });

    $scope.toggleLogin = function() {
        if(user.isLoggedIn()) {
            user.logout().then(function() {
                $scope.$apply();
                $location.path('/lists').search({ dontlogin: true });
                $route.reload();
            });
        }
        else {
            user.login_user().then(function() {
                $scope.$apply();
            });
        }
    };

    $scope.editProfile = function() {
        if(user.isLoggedIn()) { // just in case
            user.editProfile().then(function() {
                $scope.$apply();
            });
        }
    };

    $scope.editSettings = function() {
        editorOptions.change();
    };

    clearNavBar();
    user.refreshSession($location.search());

}]);
