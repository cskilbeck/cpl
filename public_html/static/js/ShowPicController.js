(function() {
    "use strict";

    mainApp.controller('ShowPicController', ['$scope', '$uibModal', 'user', 'ajax', '$rootScope', 'status', '$location', '$route', '$window', '$routeParams', 'dialog',
    function($scope, $uibModal, user, ajax, $rootScope, status, $location, $route, $window, $routeParams, dialog) {

        $scope.present_id = $routeParams.present_id;

        $scope.goback = function() {
            $window.history.back();
        };

    }]);
})();
