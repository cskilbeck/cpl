//////////////////////////////////////////////////////////////////////

var mainApp = angular.module('mainApp', ['ngRoute', 'ngAnimate', 'ui.bootstrap', 'ngResource', 'ngTouch']);

mainApp.isMobile = /iPhone|iPad|iPod|Android/i.test(navigator.userAgent);

//////////////////////////////////////////////////////////////////////
// collapse navbar when a nav link is clicked

$(document).on('click','.navbar-collapse.in',function(e) {

    if( $(e.target).is('a') && ( $(e.target).attr('class') != 'dropdown-toggle' ) ) {
        $(this).collapse('hide');
    }

});

//////////////////////////////////////////////////////////////////////
// for nabbing files from input file selector

mainApp.directive('appFilereader', ['$q', function($q) {
    return {
        restrict: 'A',
        require: '?ngModel',
        link: function(scope, element, attrs, ngModel) {
            if (ngModel) {
                ngModel.$render = function() {};
                element.bind('change', function(e) {
                    var element = e.target;
                    $q.all(Array.prototype.slice.call(element.files, 0).map(readFile))
                        .then(function(values) {
                            if (element.multiple) {
                                ngModel.$setViewValue(values);
                            }
                            else {
                                ngModel.$setViewValue(values.length ? values[0] : null);
                            }
                        });

                    function readFile(file) {
                        var deferred = $q.defer();
                        var reader = new FileReader();
                        reader.onload = function(e) {
                            deferred.resolve(e.target.result);
                        };
                        reader.onerror = function(e) {
                            deferred.reject(e);
                        };
                        reader.readAsDataURL(file);
                        return deferred.promise;
                    }
                }); //change
            } //if(ngModel)
        } //link
    }; //return
}]);

//////////////////////////////////////////////////////////////////////
// dblClick which works on touch

mainApp.directive('iosDblclick',
    function () {

        const DblClickInterval = 300; //milliseconds

        var firstClickTime;
        var waitingSecondClick = false;

        return {
            restrict: 'A',
            link: function (scope, element, attrs) {
                element.bind('click', function (e) {

                    if (!waitingSecondClick) {
                        firstClickTime = (new Date()).getTime();
                        waitingSecondClick = true;

                        setTimeout(function () {
                            waitingSecondClick = false;
                        }, DblClickInterval);
                    }
                    else {
                        waitingSecondClick = false;

                        var time = (new Date()).getTime();
                        if (time - firstClickTime < DblClickInterval) {
                            e.preventDefault();
                            scope.$event = e;
                            scope.$apply(attrs.iosDblclick);
                        }
                    }
                });
            }
        };
    });

//////////////////////////////////////////////////////////////////////

// TODO (chs):
// TODO (chs): 
// TODO (chs): links!
// TODO (chs): public lists
// TODO (chs): picture in your profile!?
// TODO (chs): fix caching problem with image in present editor (old one shows up)
// TODO (chs): resize images (before upload?) and check size on server
// TODO (chs): get rid of dontlogin query parameter, use a var in the user service instead
// TODO (chs): ? shouldn't be able to share a list with yourself (but it's handy for testing)
// TODO (chs): deal with: user is signed in with email X and is sent a share code to email Y (which they also use) - maybe share it with the existing user_id? they got the email, after all
// TODO (chs): drag/drop links to create entries in lists (even on mobile with touch? press'n'hold to grab?)
// TODO (chs): when a list is deleted (for good, not just marked for deletion), delete all its links (and the orphaned presents too)
// TODO (chs): undelete a present/link (sigh)
// TODO (chs): drag/drop to order presents in a list
// TODO (chs): restore deleted lists, presents
// TODO (chs): for pete's sake use a LESS or SASS file for the style (and bower/npm for everything)

//////////////////////////////////////////////////////////////////////

// NOPE (chs): move a present to another list
// NOPE (chs): copy a present to another list

//////////////////////////////////////////////////////////////////////

// DONE (chs): make navmenu go away when clicked
// DONE (chs): get rid of the status bar and the fixed layout crap [sort of, good enough for now]
// DONE (chs): shopping list view (all the presents I've reserved)
// DONE (chs): share a list (send email and create target user)
// DONE (chs): pressing enter into share email input field should click the tick
// DONE (chs): proper share dialog (show current sharing status, allow add and remove sharing entries)
// DONE (chs): view some else's list that's shared with you (or is public)
// DONE (chs): bag a present when viewing someone else's list
// DONE (chs): add a picture to a present (file upload: https://github.com/thejimmyg/wsgi-file-upload/blob/master/upload.py)
// DONE (chs): get rid of flex formatting, use BS grid
// DONE (chs): picture upload from mobile camera
// DONE (chs): make layout responsive / mobile friendly
// DONE (chs): fix image orientation problem (when pic taken on a phone)
// DONE (chs): make a thumbnail on the server and serve that
// DONE (chs): clicking on thumbnail shows full size image
// DONE (chs): show pictures wherever there's a present (and click to showpic)
