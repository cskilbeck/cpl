DROP DATABASE IF EXISTS `CPL`;

CREATE DATABASE `CPL` /*!40100 DEFAULT CHARACTER SET utf8 */;

-- users
-- a user can be created without their knowledge when a list is shared with them, in which case name & password will be null
-- to create a list or add presents, a user must be logged in

USE `CPL`;

CREATE TABLE `users` (
    `user_id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
    `user_email` VARCHAR(255) CHARACTER SET latin1 NOT NULL,
    `user_username` VARCHAR(200) NOT NULL,
    `user_password` VARCHAR(100) CHARACTER SET latin1 NOT NULL,
    `user_created` DATETIME DEFAULT NULL,
    PRIMARY KEY (`user_id`),
    UNIQUE KEY `user_email` (`user_email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- presents they want

CREATE TABLE `presents` (
    `present_id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
    `user_id` INT(10) UNSIGNED NOT NULL,
    `present_name` VARCHAR(255) NOT NULL,
    `present_description` VARCHAR(2048),
    `present_link` VARCHAR(2048),
    `present_created` DATETIME DEFAULT NULL,
    `present_reserved_by` INT(10) UNSIGNED DEFAULT NULL,
    `present_reserved_at` DATETIME DEFAULT NULL,
    PRIMARY KEY (`present_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- lists of presents

CREATE TABLE `lists` (
    `list_id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
    `list_name` VARCHAR(200) NOT NULL,
    `user_id` INT(10) UNSIGNED NOT NULL,
    `list_public` BOOLEAN DEFAULT 0,
    PRIMARY KEY (`list_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- linking presents to lists

CREATE TABLE `links` (
    `list_id` INT(10) UNSIGNED NOT NULL,
    `present_id` INT(10) UNSIGNED NOT NULL,
    PRIMARY KEY (`list_id`, `present_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- user registration reset password code admin

CREATE TABLE `resetcodes` (
    `code` INT(10) UNSIGNED NOT NULL,
    `user_email` VARCHAR(240) NOT NULL,
    `expires` DATETIME NOT NULL,
    `user_id` INT(10) UNSIGNED NOT NULL,
    PRIMARY KEY (`user_email`,`user_id`),
    UNIQUE KEY `user_UNIQUE` (`user_email`,`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- users can share their lists with other people

CREATE TABLE `shares` (
    `list_id` INT(10) UNSIGNED NOT NULL,        -- the list
    `user_id` INT(10) UNSIGNED,                 -- who it's shared with
    `share_code` INT(10) UNSIGNED,              -- emailed code for recipient
    `share_activated` BOOLEAN,                  -- has this share been used?
    PRIMARY KEY (`list_id`, `recipient_id`),
    UNIQUE KEY `code_UNIQUE` (`share_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
