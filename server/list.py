######################################################################

import sys, types, web, pprint, re
import MySQLdb
import dbase_nogit as DB
import mail_template
import resetcode
import api
from util import merge

######################################################################

listname = { 'type': unicode, 'min': 1, 'max': 200 }
listdesc = { 'default': u'', 'max': 240 }
email = { 'type': str, 'regex': re.compile(r"^(\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*)$") }
name = { 'type': unicode, 'min': 3, 'max': 200 }

######################################################################

@api.url('/test')
class test:

    def GET(self, db, cur):
        cur.execute('''SELECT COUNT(*) AS count FROM links''')
        return api.JSON({'found': cur.fetchone()['count']})

######################################################################
# URL: /list

@api.url('/list')
class list:

    # GET all the lists for a user_id

    @api.method(authenticate = True)
    def GET(self, db, cur, data):
        cur.execute('''SELECT * FROM lists_view WHERE user_id = %(user_id)s''', data)
        return api.JSON(cur.fetchall())

    # POST a new list

    @api.method(arguments = { 'name': listname, 'description': listdesc, 'public': 0 }, authenticate = True)
    def POST(self, data, db, cur):
        if data['description'] is not None and len(data['description']) == 0:
            data['description'] = None
        cur.execute('''INSERT INTO lists (list_name, list_description, user_id, list_public, list_lastsaved)
                        VALUES(%(name)s, %(description)s, %(user_id)s, %(public)s, NOW())''', data)
        return api.JSON({ 'list_id': cur.lastrowid })

######################################################################
# URL: /list/share/{list_id}

@api.url('/list/share/(.*)')
class list_share:

    # TODO (chs): make a regex for delimited email address lists (and check how many there are)

    @api.method(authenticate = True, arguments = { 'add': u"", 'del': u"" })
    def POST(self, id, db, cur, data):
        api.check(id, int)

        # get the email addresses

        print data

        adds = data['add'].split(';') if len(data['add']) > 0 else [];
        dels = data['del'].split(';') if len(data['del']) > 0 else [];

        # keep count of what happens

        deleted = 0
        added = 0

        # delete the deletions (cannot undo this)
        # TODO (chs): make 'delete share' undoable somehow (deleted field on shares and an active_shares view?)

        if(len(dels) > 0):
            cur.execute('''DELETE shares FROM shares
                            JOIN lists ON lists.list_id = shares.list_id
                            JOIN users ON users.user_id = shares.user_id
                            WHERE lists.user_id = {0}
                            AND shares.list_id = {1}
                            AND user_email IN ({2})'''.format('%s', '%s', ','.join(['%s'] * len(dels))), (data['user_id'], int(id)) + tuple(dels))
            deleted = cur.rowcount

        # add the new ones

        print adds

        for a in adds:
            recipient_id = 0
            name = ''
            try:

                # create zombie user for recipient
                cur.execute('''INSERT INTO users (user_email, user_created) VALUES (%s, NOW())''', (a,))
                recipient_id = cur.lastrowid

            except MySQLdb.IntegrityError:
                # or get details of existing user
                cur.execute('''SELECT user_id, user_username FROM users WHERE user_email = %s''', (a,))
                row = cur.fetchone()
                recipient_id = row['user_id']
                if row['user_username'] is not None:
                    name = row['user_username']

            rc = resetcode.unique(cur, "shares", "share_code")
            try:
                # create the share
                cur.execute('''INSERT INTO shares (list_id, user_id, share_code) VALUES (%s, %s, %s)''', (int(id), recipient_id, rc))
                added += 1

            except MySQLdb.IntegrityError:
                # already shared, do nothing
                pass

            else:
                # get details of new or old user
                cur.execute('''SELECT user_username AS cc_name, user_email AS cc_email FROM users WHERE user_id = %(user_id)s''', data)
                row = cur.fetchone()

                # send a mail to let them know list has been shared with them
                row['username'] = name
                row['email'] = a
                row['link'] = 'http://{0}/view/{1}?code={2}'.format(DB.Vars.site, id, resetcode.as_str(rc))
                mail_template.send(mail_template.list_shared, row)

        return api.JSON({'added': added, 'deleted': deleted })

######################################################################
# URL: /list/shared

@api.url('/list/shared')
class list_shared:

    @api.method(authenticate = True)
    def GET(self, db, cur, data):
        cur.execute('''SELECT * FROM shared_lists WHERE viewer_id=%(user_id)s''', data)
        return api.JSON(cur.fetchall())

######################################################################
# URL: /list/mine/{list_id}

@api.url('/list/mine/(.*)')
class list_mine:

    # GET contents of one of my lists

    @api.method(authenticate = True)
    def GET(self, id, db, cur, data):
        p = merge(data, id = id)
        cur.execute('''SELECT * FROM lists_view WHERE list_id=%(id)s''', p)
        row = cur.fetchone()
        api.err_if(row is None, '404 no such list')
        cur.execute('''SELECT * FROM present_list WHERE list_id = %(id)s''', p)
        return api.JSON({ 'details': row, 'presents': cur.fetchall() })

######################################################################
# URL: /list/undelete/{list_id}

@api.url('/list/undelete/(.*)')
class list_undelete:

    # POST undelete a list

    @api.method(authenticate=True)
    def POST(self, id, db, cur, data):
        api.check(id, int)
        p = merge(data, id = id)
        cur.execute('''UPDATE lists SET list_deleted = NULL WHERE list_id=%(id)s AND user_id=%(user_id)s''', p)
        return api.JSON({ 'undeleted': cur.rowcount })

######################################################################
# URL: /list/public/{list_id}

@api.url('/list/public/(.*)')
class list_public_details:

    # GET details of a public list

    @api.method()
    def GET(self, id, db, cur, data):
        api.check(id, int)
        cur.execute('''SELECT * FROM lists_view WHERE list_id = %s AND lists_view.list_public != 0''', (id,))
        rows = cur.fetchall()
        api.err_if(len(rows) == 0, '404 no such list')
        return api.JSON(cur.fetchall())

######################################################################

# URL: /shop

@api.url('/list/shop')
class shop:

    @api.method(authenticate = True)
    def GET(self, db, cur, data):
        cur.execute('''SELECT present_name, present_description, present_id, user_username, present_haspic FROM present_list
                        JOIN users ON users.user_id = present_list.user_id
                        WHERE present_reserved_by = %(user_id)s
                        ORDER BY users.user_id ASC''', data)
        return api.JSON(cur.fetchall())

######################################################################

@api.url('/list/view/(.*)')
class list_view:

    # get contents of a list (if it's public or shared with you and you're logged in)
    @api.method()
    def GET(self, id, db, cur, data):
        api.check(id, int)
        data['id'] = id
        if data['user_id'] != 0:
            cur.execute('''SELECT COUNT(*) AS count FROM shares WHERE list_id=%(id)s AND user_id=%(user_id)s''', data)
            api.err_if(cur.fetchone()['count'] == 0, '401 List is not shared with you')
        else:
            cur.execute('''SELECT COUNT(*) AS count FROM lists_view WHERE list_id=%(id)s and list_public != 0''', data)
            api.err_if(cur.fetchone()['count'] == 0, '401 List is not public')
        results = {}
        cur.execute('''SELECT * FROM present_list WHERE list_id=%(id)s''', data)
        results['list'] = cur.fetchall()
        cur.execute('''SELECT list_name, list_description, user_username
                        FROM lists_view
                            JOIN users ON lists_view.user_id = users.user_id
                        WHERE list_id=%(id)s''', data)
        results['details'] = cur.fetchone()
        return api.JSON(results)

######################################################################
# URL: /list/mine

@api.url('/list/mine')
class list_all:

    # GET list of of my lists

    @api.method(authenticate = True)
    def GET(self, db, cur, data):
        cur.execute('''SELECT * FROM lists_view WHERE user_id=%(user_id)s''', data)
        return api.JSON(cur.fetchall())

######################################################################
# URL: /list/contents/{list_id}

@api.url('/list/contents/(.*)')
class list_contents:

    # GET the contents of a list

    @api.method(authenticate = True)
    def GET(self, id, db, cur, data):
        api.check(id, int)
        cur.execute('''SELECT * FROM lists_view WHERE list_id=%(id)s AND user_id=%(user_id)s''', merge(data, id = id))
        return api.JSON(cur.fetchall())

######################################################################

@api.url('/list/sharedwith/(.*)')
class list_shared_with:

    @api.method(authenticate = True)
    def GET(self, list_id, cur, db, data):
        cur.execute('''SELECT list_name FROM lists_view WHERE list_id=%s''', (list_id,))
        api.err_if(cur.rowcount == 0, '404 no such list')
        list_name = cur.fetchone()['list_name']
        cur.execute('''SELECT shares.user_id AS recipient, user_username, user_email
                        FROM shares
                            JOIN users ON users.user_id = shares.user_id
                            JOIN lists ON lists.list_id = shares.list_id
                        WHERE shares.list_id = %s
                            AND lists.user_id = %s''', (list_id, data['user_id']))
        return api.JSON({'shares': cur.fetchall(), 'list_name': list_name })

######################################################################

@api.url('/list/(.*)')
class list_withid:

    # PUT updated details into a list

    @api.method(arguments = { 'name': listname, 'description': listdesc, 'public': 0 }, authenticate = True)
    def PUT(self, id, data, db, cur):
        cur.execute('''UPDATE lists
                        SET list_name=%(name)s,
                            list_public=%(public)s,
                            list_description=%(description)s
                        WHERE list_id=%(id)s AND user_id=%(user_id)s''', merge(data, id = id))
        return api.JSON({ 'list_id': id })

    # DELETE a single list (by list_id)

    @api.method(authenticate = True)
    def DELETE(self, id, db, cur, data):
        api.check(id, int)
        cur.execute('''UPDATE lists SET list_deleted = NOW() WHERE user_id=%(user_id)s AND list_id=%(id)s''', merge(data, id = id))
        return api.JSON({'deleted': cur.rowcount})
