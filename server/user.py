import sys, types, os, time, datetime, struct, re, random
import web, pprint, json, iso8601, unicodedata, urlparse, urllib
import bcrypt
import dbase_nogit as DB
import JWT
import mail_template
import resetcode
import api

def create_token(payload, lifetime = 60 * 60 * 24 * 30):
    return JWT.create(DB.Vars.secret, payload, lifetime)

def checkPassword(hashed, password):
    try:
        attempt = bcrypt.hashpw(password, hashed)
        api.err_if(attempt != hashed, '401 Incorrect password')
    except ValueError:
        api.error('401 Incorrect password!')

email = { 'type': str, 'regex': re.compile(r"^(\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*)$") }
password = { 'type': unicode, 'min': 6, 'max': 64 }
optionalpassword = { 'type': unicode, 'optional': True, 'min': 6, 'max': 64 }
username = { 'type': unicode, 'min': 3, 'max': 32 }
resetcodeparam = { 'type': str, 'regex': resetcode.regex }
optionalresetcode = { 'type': str, 'optional': True, 'regex': resetcode.regex }

######################################################################

# /api/user/modify

# TODO (chs): make this 2 seperate APIs - one for 'with a reset code' and one for 'user initiated'

@api.url('/user/modify')
class details:

    @api.method(
        arguments = {
            'uid': int,
            'code': optionalresetcode,
            'email': email,
            'oldpassword': optionalpassword,
            'password': optionalpassword,
            'username': username
        })
    def POST(self, db, cur, data):
        # check resetcode valid
        if data['oldpassword'] is None:
            api.err_if(data['code'] is None, '401 need a password or reset code')
            data['resetcode'] = resetcode.as_long(data['code'])
            cur.execute('''SELECT COUNT(*) as count
                            FROM resetcodes
                            WHERE user_id = %(uid)s
                            AND code = %(resetcode)s
                            AND expires > NOW()''', data)
            api.err_if(cur.fetchone()['count'] != 1, '401 Reset code bad or expired')

        # get current user details
        cur.execute('''SELECT * FROM users WHERE user_id = %(uid)s''', data)
        api.err_if(cur.rowcount != 1, '401 Incorrect user_id')
        user = cur.fetchone()

        # check old password if supplied
        if data['oldpassword'] is not None:
            checkPassword(user['user_password'], data['oldpassword'])

        # changing email address? if so, check if new one is available
        if data['email'] != user['user_email']:
            cur.execute('''SELECT COUNT(*) AS count FROM users WHERE user_email=%(email)s''', data)
            api.err_if(cur.fetchone()['count'] != 0, '409 Email already taken')

        # changing username? if so, check if new one is available
        if data['username'] != user['user_username']:
            cur.execute('''SELECT COUNT(*) AS count FROM users WHERE user_username=%(username)s''', data)
            api.err_if(cur.fetchone()['count'] != 0, '409 Username already taken')

        # changing password? update everything
        if data['password'] is not None:
            data['hashed'] = bcrypt.hashpw(data['password'], bcrypt.gensalt(12))
            cur.execute('''UPDATE users
                            SET user_email = %(email)s,
                                user_password = %(hashed)s,
                                user_username = %(username)s
                            WHERE user_id = %(uid)s''', data)
        else:
            # or just update email/username
            cur.execute('''UPDATE users
                            SET user_email = %(email)s,
                                user_username = %(username)s
                            WHERE user_id = %(uid)s''', data)
            data['hashed'] = user['user_password']

        # if anything changed, send a mail about it
        if cur.rowcount == 1:
            mail_template.send(mail_template.details_changed, data);

        # delete all reset codes for this user
        cur.execute('''DELETE FROM resetcodes WHERE user_id = %(uid)s''', data)

        # results
        return api.JSON({
                'changed': cur.rowcount == 1,
                'token': create_token({ 'user_id': data['uid'] }),
                'user_id': data['uid'],
                'user_username': data['username'],
                'user_email': data['email']
            })

######################################################################
# /api/createprofile

# create a profile from a code

@api.url('/user/createprofile')
class createprofile:

    @api.method(arguments = { 'password': password, 'username': username, 'code': resetcode })
    def POST(self, db, cur, data):
        data['resetcode'] = resetcode.as_long(data['code'])
        cur.execute('''SELECT * FROM shares WHERE share_code=%(resetcode)s AND share_activated IS NULL''', data)
        api.err_if(cur.rowcount < 1, '404 Unknown share code')
        data['hashed'] = bcrypt.hashpw(data['password'], bcrypt.gensalt(12))
        row = cur.fetchone()
        data['uid'] = row['user_id']
        cur.execute('''UPDATE users SET user_username=%(username)s, user_password=%(hashed)s WHERE user_id=%(uid)s''', data)
        api.err_if(cur.rowcount != 1, "505 Couldn't create profile!?")
        cur.execute('''UPDATE shares SET share_activated=NOW() WHERE share_code=%(resetcode)s''', data)
        cur.execute('''SELECT user_email FROM users WHERE user_id=%(uid)s''', data)
        row = cur.fetchone()
        result = {}
        result['user_id'] = data['uid']
        result['user_username'] = data['username']
        result['token'] = create_token({ 'user_id': result['user_id'] })
        result['email'] = row['user_email']
        data['name'] = data['username']
        data['email'] = row['user_email']
        mail_template.send(mail_template.welcome, data);
        return api.JSON(result)

######################################################################
# /api/register

@api.url('/user/register')
class register:

    @api.method(arguments = { 'email': email, 'password': password, 'username': username, 'code': optionalresetcode })
    def POST(self, db, cur, data):
        result = {}
        cur.execute('''SELECT COUNT(*) AS count FROM users WHERE user_email=%(email)s''', data)
        api.err_if(cur.fetchone()['count'] != 0, '409 Email already taken')
        cur.execute('''SELECT COUNT(*) AS count FROM users WHERE user_username=%(username)s''', data)
        api.err_if(cur.fetchone()['count'] != 0, '409 Username already taken')
        data['hashed'] = bcrypt.hashpw(data['password'], bcrypt.gensalt(12))
        cur.execute('''INSERT INTO users (user_email, user_password, user_username, user_created)
                        VALUES (%(email)s, %(hashed)s, %(username)s, NOW() )
                        ON DUPLICATE KEY
                        UPDATE user_password=%(hashed)s, user_username=%(username)s''', data)
        api.err_if(cur.rowcount != 1, "401 Can't create account")
        result['user_id'] = cur.lastrowid
        result['user_username'] = data['username']
        result['token'] = create_token({ 'user_id': result['user_id'] })
        data['name'] = data['username']
        mail_template.send(mail_template.welcome, data);
        return api.JSON(result)

######################################################################

@api.url('/user/details')
class userdetails:

    @api.method(arguments = { 'email': email, 'code': resetcodeparam })
    def GET(self, db, cur, data):
        data['resetcode'] = resetcode.as_long(data['code'])
        cur.execute('''SELECT * FROM resetcodes WHERE code = %(resetcode)s AND expires > NOW()''', data)
        api.err_if(cur.rowcount != 1, '404 reset code not found')
        cur.execute('''SELECT user_username, user_id as uid FROM users WHERE user_email = %(email)s''', data)
        row = cur.fetchone()
        api.err_if(row is None, '404 Email not found or bad code')
        return api.JSON(row)

######################################################################

# /api/resetpw

@api.url('/user/resetpw')
class resetpw:

    @api.method(arguments = { 'email': email })
    def POST(self, db, cur, data):
        cur.execute('''SELECT user_id, user_username FROM users WHERE user_email = %(email)s''', data)
        api.err_if(cur.rowcount == 0, '404 Email not found')
        row = cur.fetchone()
        data['code'] = resetcode.unique(cur, "resetcodes", "code")
        print "Generated reset code", resetcode.as_str(data['code']), '(' + str(data['code']) + ')'
        data['user_id'] = row['user_id']
        cur.execute('''INSERT INTO resetcodes (user_id, code, expires)
                        VALUES (%(user_id)s, %(code)s, NOW() + INTERVAL 1 HOUR)
                        ON DUPLICATE KEY
                        UPDATE code = %(code)s, expires = NOW() + INTERVAL 1 HOUR''', data)
        api.err_if(cur.rowcount < 1, "500 can't generate reset code!?")
        data['link'] = "http://{2}?resetpassword={0}&email={1}".format(resetcode.as_str(data['code']), urllib.quote(data['email']), DB.Vars.site)
        data['username'] = row['user_username']
        mail_template.send(mail_template.password_reset, data)
        return api.JSON({ 'email_sent': True })

######################################################################

# /api/refreshSession

@api.url('/user/refreshSession')
class refreshSession:

    @api.method(authenticate = True)
    def POST(self, db, cur, data):
        cur.execute('''SELECT user_id, user_email, user_username FROM users WHERE user_id = %(user_id)s''', data)
        api.err_if(cur.rowcount != 1, '401 no such user')
        row = cur.fetchone()
        row['token'] = create_token(data)
        return api.JSON(row)

######################################################################

# /api/login

@api.url('/user/login')
class login:

    @api.method(arguments = {'email': email, 'password': password })
    def POST(self, db, cur, data):
        cur.execute('''SELECT user_id, user_password, user_username, user_email FROM users WHERE user_email = %(email)s''', data)
        api.err_if(cur.rowcount != 1, '401 Incorrect email address')
        row = cur.fetchone()
        checkPassword(row['user_password'], data['password'])
        row['token'] = create_token({ 'user_id': row['user_id'] })
        row.pop('user_password')
        return api.JSON(row)


@api.url('/user/code/(.*)')
class user_code:

    @api.method()
    def GET(self, code, db, cur, data):
        # look for a code in the shares table
        # that's the user!
        # if that user is already a good'un
        pass

@api.url('/user/fromcode/(.*)')
class user_fromcode:

    @api.method()
    def GET(self, code, db, cur, data):
        #api.check(code, resetcode.regex)   # TODO (chs): make api.check use the proper paramcheck stuff
        data['code'] = resetcode.as_long(code)
        cur.execute('''SELECT list_id, users.user_id, user_email, user_username
                        FROM shares
                            INNER JOIN users ON users.user_id = shares.user_id
                        WHERE share_code=%(code)s''', data)
        row = cur.fetchone()
        api.err_if(row is None, '404 no such code')
        return api.JSON(row)
