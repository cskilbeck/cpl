import dbase_nogit as DB
import mail

welcome = {
    'sender_name'   : '{0}'.format(DB.Vars.name),
    'sender_address': 'admin@{0}'.format(DB.Vars.site),
    'subject'       : '%(username)s, welcome to {0}'.format(DB.Vars.name),
    'html'          : '''Hello %(username)s,<br>
<p>Congratulations, you've registered your account at <a href='http://{0}'>http://{0}</a>.</p>
<p>Thanks,<br>
The {1} team.</p>'''.format(DB.Vars.site, DB.Vars.name)
}

details_changed = {
    'sender_name'   : '{0}'.format(DB.Vars.name),
    'sender_address': 'admin@{0}'.format(DB.Vars.site),
    'subject'       : '%(username)s details updated at {0}'.format(DB.Vars.name),
    'html'          : '''Hello %(username)s,<br>
<p>Your details at {0} have been updated.</p>
<p>Thanks,<br>
The {0} team.</p>'''.format(DB.Vars.name)
}

password_reset = {
    'sender_name'   : '{0}'.format(DB.Vars.name),
    'sender_address': 'admin@{0}'.format(DB.Vars.site),
    'subject'       : 'Password reset for %(username)s at {0}'.format(DB.Vars.name),
    'html'          : '''Hello %(username)s,<br>
<p>PASSWORD RESET REQUEST</p>
<p>Someone has requested a password reset for an account on {0} with this email address.<br>
If this wasn't you, please ignore this email.<br>
Otherwise, you can visit this link to reset your password: <a href=%(link)s>%(link)s</a></p>
<p>Thanks<br>
The {0} team.</p>'''.format(DB.Vars.name)
}

list_shared = {
    'sender_name'   : '{0}'.format(DB.Vars.name),
    'sender_address': 'admin@{0}'.format(DB.Vars.site),
    'subject'       : '%(cc_name)s has shared their Christmas Present List with you',
    'html'          : '''Hello!<br>
<p>%(cc_name)s has made a Christmas Present List. You can see it by clicking this link:</p>
<p><a href=%(link)s>%(link)s</a></p>
<p>Thanks<br>
The {0} team.</p>'''.format(DB.Vars.name)
}

def send(template, params):
    mail.send(template['sender_name'],
                template['sender_address'],
                params.get('username'),
                params['email'],
                params.get('cc_name'),
                params.get('cc_email'),
                template['subject'] % params,
                template['html'] % params)
