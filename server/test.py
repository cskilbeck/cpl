#!/usr/bin/env python

import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.Header import Header
from email.Utils import parseaddr, formataddr
import threading
import re

mailuser    = 'EUROPE\chs'
mailpwd     = 'XXXXX'

striplinks = re.compile(r'<a.*?>(.*?)</a>')
stripparastart = re.compile(r'<p>')
stripparaend = re.compile(r'</p>')
stripbreak = re.compile(r'<br>')

def html2text(x):
    x = re.sub(striplinks, r'\1', x)
    x = re.sub(stripparastart, '', x)
    x = re.sub(stripparaend, '\n\n', x)
    x = re.sub(stripbreak, '\n', x)
    return x

def _send(fromname, fromaddr, name, address, ccname, ccaddress, subject, html):

    header_charset = 'ISO-8859-1'

    for body_charset in 'US-ASCII', 'ISO-8859-1', 'UTF-8':
        try:
            html.encode(body_charset)
        except UnicodeError:
            pass
        else:
            break

    sender_name = str(Header(unicode(fromname), header_charset))
    recipient_name = str(Header(unicode(name), header_charset))

    fromaddr = fromaddr.encode('ascii')
    address = address.encode('ascii')

    msg = MIMEMultipart('alternative')
    msg['From'] = formataddr((sender_name, fromaddr))
    msg['To'] = formataddr((recipient_name, address))
    msg['Subject'] = Header(unicode(subject), header_charset)

    addresses = [address]

    if ccaddress is not None and ccname is not None:
        ccaddress = ccaddress.encode('ascii')
        msg['CC'] = formataddr((str(Header(unicode(ccname), header_charset)), ccaddress))
        addresses.append(ccaddress)

    part1 = MIMEText(html2text(html).encode(body_charset), 'plain', body_charset)
    part2 = MIMEText(('<html><head></head><body>%s</body></html>' % html).encode(body_charset), 'html', body_charset)
    msg.attach(part1)
    msg.attach(part2)

    s = smtplib.SMTP('cloudmail.microsoft.com', 587, "outlook.com", 20)
    s.ehlo()
    s.starttls()
    s.ehlo()
    s.login(mailuser, mailpwd)
    s.sendmail(fromaddr, addresses, msg.as_string())
    s.quit()
    print "Sent mail to", addresses, 'from', fromaddr

def send(fromname, fromaddr, toname, toaddress, ccname, ccaddress, subject, htmlbody):
    threading.Thread(target = _send, args = (fromname, fromaddr, toname, toaddress, ccname, ccaddress, subject, htmlbody)).start()

send("Charlie Skilbeck", "charlie@xbox.com", "Charlie Skilbeck", "charlie@skilbeck.com", "Will Leach", "will@xbox.com", "Via exchange!?", "This email was sent via our exchange SMTP server...")
