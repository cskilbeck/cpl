######################################################################

import os, sys, types, web, pprint, re, base64
import dbase_nogit as DB
import api
from util import merge
from cStringIO import StringIO
from PIL import Image, ExifTags
import imghdr

######################################################################
# some arg specs

presentname     = { 'type': unicode, 'min': 1, 'max': 200 }
presentdesc     = { 'type': unicode, 'optional': True, 'max': 240 }
presentlink     = { 'type': unicode, 'optional': True }
arg_picture     = { 'type': str, 'optional': True, 'default': None }
arg_update_pic  = { 'type': int, 'optional': True, 'default': 0 }
list_id         = { 'type': int, 'min': 1 }

# map MIME type to file extension

extensions = {
    'PNG': 'png',
    'JPEG': 'jpg',
    'BMP': 'bmp'
}

######################################################################
# get filename for an image type/id

def get_image_filename(id, fldr):
    return os.path.join(api.root(), '../public_html/' + fldr + '/' + str(id))

######################################################################
# get image data and type from a URL content string

def get_image(img):
    pic = re.match('data:image/(.*);base64,(.*)', img)  # TODO (chs): fix this potential perf problem
    picdata = None
    if pic is not None:
        try:
            picdata = base64.b64decode(pic.group(2))
        except TypeError as t:
            api.error('415 Image decode failed')

    # save the image if there was one
    return picdata

######################################################################
# delete an image from the pics folder

def delete_image(id):

    def delete_file(name):
        try:
            os.remove(name)
        except OSError: # probly file not found
            pass

    delete_file(get_image_filename(id, 'pics'))
    delete_file(get_image_filename(id, 'thumbnails'))

######################################################################
# save an image to the pics folder

# TODO (chs): maybe thread image save so request can go straight back, assuming it saved ok (deal with file write error how?)

def save_image(picdata, id):
    try:
        data = StringIO(picdata)
        imgtype = imghdr.what(None, picdata)
        print "It's a", imgtype
        image = Image.open(data)
        try:
            exif = image._getexif()
            if exif is not None:
                orient = exif[274]
                if orient == 3:
                    image = image.rotate(180, expand = True)    # Lossy rotate, but fuck it
                elif orient == 6:
                    image = image.rotate(270, expand = True)    # and also, now the exif data is wrong, but fuck that too
                elif orient == 8:
                    image = image.rotate(90, expand = True)
        except (KeyError, IndexError, AttributeError):
            pass
        image.save(get_image_filename(id, 'pics'), imgtype)
        image.thumbnail((96, 96), Image.ANTIALIAS)
        image.save(get_image_filename(id, 'thumbnails'), "JPEG", subsampling = 0, quality = 100)
    except IOError as f:
        delete_image(id)
        api.error('500 error saving image')

######################################################################
# URL /present

@api.url('/present')
class present:

    # POST a new present

    @api.method(authenticate = True,
                arguments = {
                    'present_name': presentname,
                    'present_description': presentdesc,
                    'present_link': presentlink,
                    'present_pic': arg_picture,
                    'list_id': list_id
                })
    def POST(self, db, cur, data):

        picdata = None
        if(data['present_pic']):
            picdata = get_image(data['present_pic'])

        data['present_haspic'] = picdata is not None

        cur.execute('''INSERT INTO presents (present_name, present_description, present_link, present_haspic, user_id)
                        VALUES (%(present_name)s, %(present_description)s, %(present_link)s, %(present_haspic)s, %(user_id)s)''', data)
        data['present_id'] = cur.lastrowid

        # link it up with the list
        cur.execute('''INSERT INTO links (list_id, present_id)
                        VALUES (%(list_id)s, %(present_id)s)''', data)

        if picdata is not None:
            save_image(picdata, data['present_id'])

        return api.JSON({ 'present_id': data['present_id'] })

######################################################################
# URL /present/reserve/present_id

@api.url('/present/reserve/(.*)')
class present_reserve:

    @api.method(authenticate = True)
    def POST(self, id, db, cur, data):
        api.check(id, int)
        data['id'] = id
        cur.execute('''UPDATE presents
                        SET present_reserved_by = %(user_id)s,
                            present_reserved_at = NOW()
                        WHERE present_id = %(id)s''', data)
        return api.JSON({ 'reserved': cur.rowcount })

######################################################################
# URL /link/delete/{present_id}/{list_id}

@api.url('/link/delete/(.*)/(.*)')
class link_delete:

    @api.method(authenticate = True)
    def POST(self, present_id, list_id, db, cur, data):
        api.check(present_id, int)
        api.check(list_id, int)
        data['present_id'] = int(present_id)
        data['list_id'] = int(list_id)

        # check the current user owns the list in which the link is
        cur.execute('''SELECT COUNT(*) AS count FROM lists_view WHERE list_id=%(list_id)s AND user_id=%(user_id)s''', data)
        api.err_if(cur.fetchone()['count'] == 0, '404 no such list')

        # unlink the present from the list
        cur.execute('''DELETE FROM links WHERE present_id=%(present_id)s AND list_id=%(list_id)s''', data)
        rc = { 'deleted': cur.rowcount }

        # check if the present is an orphan now
        cur.execute('''SELECT COUNT(*) AS count FROM present_list WHERE user_id=%(user_id)s AND present_id=%(present_id)s''', data)
        if cur.fetchone()['count'] == 0:

            # delete the pic
            delete_image(present_id)

            # and delete the present
            cur.execute('''DELETE FROM presents WHERE present_id=%(present_id)s''', data)

        # return how many were deleted (0 or 1)
        return api.JSON(rc)

######################################################################
# URL /present/release/present_id

@api.url('/present/release/(.*)')
class present_release:

    @api.method(authenticate = True)
    def POST(self, id, db, cur, data):
        api.check(id, int)
        data['id'] = id
        cur.execute('''UPDATE presents
                        SET present_reserved_by = NULL,
                            present_reserved_at = NULL
                        WHERE present_id = %(id)s
                        AND present_reserved_by = %(user_id)s''', data)
        return api.JSON({ 'released': cur.rowcount })


######################################################################
# URL /present/{present_id}

@api.url('/present/(.*)')
class present_modify:

    # PUT new present details

    @api.method(authenticate = True,
                arguments = {
                    'present_name': presentname,
                    'present_description': presentdesc,
                    'update_pic': arg_update_pic,       # if this is true
                    'present_pic': arg_picture,         # then this can be a pic or None
                    'present_link': presentlink
                })
    def PUT(self, id, db, cur, data):
        api.check(id, int)

        data['id'] = id

        # picture options:
        #   leave pic alone (whether there is one or not) - update_pic = 0
        #   add new pic - present_pic == a picture, update_pic = 1
        #   replace existing with new pic - present_pic == a picture, update_pic = 1
        #   delete existing pic - present_pic == None, update_pic = 1

        pct = ''

        if data['update_pic']:
            delete_image(id)
            picdata = None
            if data['present_pic']:
                picdata = get_image(data['present_pic'])
            pct = ' , present_haspic = %(haspic)s '
            data['haspic'] = picdata is not None

        qr = '''UPDATE presents
                SET present_name = %(present_name)s,
                    present_description = %(present_description)s,
                    present_link = %(present_link)s
                    {0}
                WHERE present_id = %(id)s'''.format(pct)

        cur.execute(qr, data)

        if data['update_pic'] and picdata is not None:
            save_image(picdata, id)

        return api.JSON({ 'saved': cur.rowcount })

######################################################################
# URL /shop

@api.url('/list/shop')
class shop:

    @api.method(authenticate = True)
    def GET(self, db, cur, data):
        cur.execute('''SELECT * FROM present_list WHERE present_reserved_by = %(user_id)s''', data)
        return api.JSON(cur.fetchall())

