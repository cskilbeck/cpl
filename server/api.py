# so, decorators without parameters create the wrapped function in __init__ (and can pass the original function parameters through)
# and decorators _with_ parameters create the wrapped function in __call__ (and can't pass the original function parameters through)

# TODO (chs): sort out parameters in the URL mingled with formdata and/or query parameters

import sys, types, os, time, datetime, struct, re, random
import web, pprint, json, iso8601, unicodedata, urlparse, urllib
import bcrypt
from contextlib import closing
import MySQLdb as mdb
import MySQLdb.cursors
import traceback
import dbase_nogit as DB
import JWT

######################################################################

_urls = []
_root = ''

######################################################################

def root(folder = None):
    global _root
    if folder is not None:
        print "Setting root to", folder
        _root = folder
    return _root

######################################################################

def _date_handler(obj):
    if hasattr(obj, 'isoformat'):
        delta = datetime.datetime.now() - datetime.datetime.utcnow()
        hh,mm = divmod((delta.days * 24*60*60 + delta.seconds + 30) // 60, 60)
        return "%s%+03d%02d" % (obj.isoformat(), hh, mm)
    else:
        return obj;

def _opendb():
    conn = mdb.connect( host        = DB.Vars.host,
                        user        = DB.Vars.user,
                        passwd      = DB.Vars.passwd,
                        db          = DB.Vars.db,
                        use_unicode = True,
                        cursorclass = MySQLdb.cursors.DictCursor,
                        charset     = 'utf8')
    conn.autocommit(True)
    return conn

######################################################################

def check(var, _type):
    try:
        _type(var)
    except (ValueError, TypeError):
        error('401 invalid parameter')

######################################################################

def JSON(x):
    # time.sleep(0.5)
    web.header('Content-type', 'application/json')
    ret = ")]}',\n" + json.dumps(x, separators = (',',':'), default = _date_handler) # Add JSONP attack defense which Angular will strip
    # print ret
    return ret

def PNG(x):
    web.header('Content-type', 'image/png')
    return x

def HTML(x):
    web.header('Content-type', 'text/html')
    return x

def ICON(x):
    web.header('Content-type', 'image/x-icon')
    return x

def BIN(x):
    web.header('Content-type', 'application/octet-stream')
    return x

def TEXT(x):
    web.header('Content-type', 'text/plain')
    return x

######################################################################

def url(path):
    def decor(cls):
        _urls.extend([path, cls.__module__ + "." + cls.__name__])
        return cls
    return decor

def urls():
    return _urls

######################################################################

def error(x):
    print x
    raise web.HTTPError(x)

def err_if(i, x):
    if(i):
        print x
        raise web.HTTPError(x)

######################################################################
# decorator to check parameter types and values in the input and authenticate user

# URI parameters will be supplied first

# -> uri                                            : parameters

# these parameters will be added:

# -> form data (and user_id if they're logged in)   : data (dictionary of form data)
# -> database                                       : db (connection), cur (cursor)

# and this is added to self:

# -> query string parameters                        : self.query (dictionary)


class method(object):

    # reser
    reserved = ('user_id',)

    def __init__(self, arguments = None, authenticate = False):
        self.paramSpec = arguments
        self.authenticate = authenticate

    def __call__(self, original_func):

        if False:
            classname = inspect.getouterframes(inspect.currentframe())[1][3]
            print "\nENDPOINT:", classname
            print "METHOD:", original_func.__name__.upper()
            print "AUTHENTICATE:", self.authenticate
            print "PARAMETERS:"
            for name in self.paramSpec:
                print pprint.pformat(name) + ":" + pprint.pformat(self.paramSpec[name])

        def new_function(slf, *args, **kwargs):

            result = {}

            # try to authenticate
            user_id = 0
            token = web.ctx.environ.get('HTTP_AUTHORIZATION')
            if token is not None and token[:7] == 'Bearer ':
                try:
                    token = JWT.extract(DB.Vars.secret, token[7:])  # TODO (chs): for auth, use a per user secret hashed with a global secret
                    payload = token.get('payload')
                    if payload is not None:
                        user_id = payload.get('user_id', 0)
                except ValueError as v:
                    pass

            # if authentication is required and not possible, 401
            if self.authenticate and user_id == 0:
                error('401 Auth required')

            # check parameters in the form data
            if self.paramSpec is not None:

                if web.ctx.method in ['GET', 'DELETE']: # TODO (chs): work out which should use URL and which should use Request Data
                    data = web.input()
                elif web.ctx.method in ['POST', 'PUT']:
                    contentType = web.ctx.env.get('CONTENT_TYPE').split(';')
                    if len(contentType) > 0:
                        if contentType[0] == 'application/json':
                            data = json.loads(web.data())
                        elif contentType[0] == 'application/x-www-form-urlencoded':
                            data = web.input()
                            # now it will be strings...
                        # TODO (chs): add more content-type handlers (I thought web.input was supposed to )
                        # DONE (chs): handle UNICODE?

                # print "ENV:", pprint.pformat(web.ctx.env)
                # print "Data:", pprint.pformat(web.data())

                params = self.paramSpec

                # TOFIX (chs): optional: True and missing parameter and type: unicode results in u'None' which should be just None

                for name in params:
                    if name in method.reserved:
                        raise ValueError('Invalid paramSpec: reserved word ' + name + ' used as an argument')
                    param = params[name]
                    optional = False
                    deftype = type(param)
                    val = data.get(name, None)
                    if deftype == type:
                        if val is None:
                            print data
                            error('401 Missing parameter %s' % (name,))
                        try:
                            val = param(val)
                        except (TypeError, ValueError):
                            raise ValueError('%s cannot be cast to %s' % (name, param.__name__))

                    elif deftype == dict:
                        optional = param.get('optional', False)
                        defval = param.get('default', None)
                        deftype = param.get('type', None)
                        if deftype is None:
                            if defval is None:
                                raise ValueError('Invalid paramSpec')
                            deftype = type(defval)
                        if not optional:
                            val = deftype(defval) if val is None else deftype(val)  # coerce to the right type (from string, usually)
                        elif val is not None:
                            val = deftype(val)

                        if val is not None:
                            minval = param.get('min', None)   # for int, float: min value, for str, min length
                            maxval = param.get('max', None)   # same as minval but max
                            regex = param.get('regex', None)

                            if regex is not None:
                                if deftype is not str:
                                    raise ValueError('Invalid paramSpec - regex supplied but {0} is not a str'.format(name))
                                re_result = re.match(regex, val)
                                if re_result is None:
                                    error('401 value for {0} is not valid'.format(name))
                                val = re_result.group(0)

                            if minval is not None:
                                if deftype in [int, float] and val < minval:
                                    error('401 value for {0} is too low'.format(name))
                                elif deftype in [str, unicode] and len(val) < minval:
                                    print val, 'is', len(val), 'long, needs to be', minval
                                    error('401 value for {0} is too short'.format(name))

                            if maxval is not None:
                                if deftype in [int, float] and val > maxval:
                                    error('401 value for {0} is too high'.format(name))
                                elif deftype in [str, unicode] and len(val) > maxval:
                                    error('401 value for {0} is too long'.format(name))

                    elif deftype in [int, float, str, unicode]:
                        val = param if val is None else deftype(val)

                    elif deftype == datetime.datetime:
                        val = param if val is None else iso8601.parse_date(val)

                    if val is None and not optional:
                        raise KeyError('Required parameter %s is missing (expected: %s)' % (name, deftype.__name__))
                    else:
                        result[name] = val

            #print pprint.pformat(result)
            result['user_id'] = user_id
            kwargs['data'] = result

            self.query = urlparse.parse_qs(urlparse.urlparse(web.ctx.query)[4], keep_blank_values=True)

            print web.ctx.method, web.ctx.path, repr(args)

            # open the database and add db,cur to kwargs
            try:
                with closing(_opendb()) as db:
                    with closing(db.cursor()) as cur:
                        kwargs['db'] = db
                        kwargs['cur'] = cur
                        return original_func(slf, *args, **kwargs)

            except ValueError as e:
                traceback.print_exc()
                print e.message
                error('404 ' + e.message)

            except KeyError as e:
                traceback.print_exc()
                print e.message
                error('404 ' + e.message)

            except mdb.Error as e:
                traceback.print_exc()
                print e.message
                error('500 Database problem')

            except web.HTTPError as e:
                raise e

            except Exception as e:
                traceback.print_exc()
                print e.message
                error('500 Unknown error')

        return new_function
