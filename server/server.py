#----------------------------------------------------------------------
# TODO (chs): separate validated api calls by url rather than flag in paramSpec
# TODO (chs): autogenerate REST docs from parameter check dictionaries
# TODO (chs): make delete undoable for N days
#
# FINISH (chs): parameter validation: min, max, minlength, maxlength, prepend, append, replace, enums?
# FINISH (chs): proper login/session thing (use JWT)
#
# DONE (chs): parameter validation and conditioning engine
# DONE (chs): DRY the REST functions
# DONE (chs): screenshots!
# DONE (chs): bcrypt password
# DONE (chs): make it unicode
#----------------------------------------------------------------------

print "\n====================================================\nServer restart\n"

import sys, types, os, web, getpass

import dbase_nogit as DB
import api
import user
import list
import present

print "Current dir is", os.getcwd()
print "PATH[0] =", sys.path[0]
print "Using database at", DB.Vars.host
print "Running in", os.path.dirname(__file__)
print "Running as", getpass.getuser()

api.root(folder = os.path.dirname(__file__))

app = web.application(api.urls(), globals())


print "====================================================\n"

application = app.wsgifunc()
